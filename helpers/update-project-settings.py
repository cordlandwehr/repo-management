#!/usr/bin/python3
import os
import sys
import yaml
import argparse
import gitlab

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Updates the Gitlab configuration based on repo-metadata')
parser.add_argument('--metadata-path', help='Path to the metadata to check', required=True)
parser.add_argument('--instance', help='Gitlab instance to work with', required=True)
parser.add_argument('--token', help='User token to use when authenticating with Gitlab', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.metadata_path ):
    print("Unable to locate specified metadata location: %s".format(args.metadata_path))
    sys.exit(1)

# Connect to Gitlab
gitlabServer = gitlab.Gitlab( args.instance, private_token=args.token )

# The following are the patterns we want to protect
branchPatternsToProtect = ['master', '*.*']

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Do we have a metadata.yaml file?
    if 'metadata.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'metadata.yaml' )
    metadataFile = open( metadataPath, 'r' )
    metadata = yaml.load( metadataFile )

    # create gitlab project instance
    project = gitlabServer.projects.get(metadata['repopath'])

    # check sanity of the description and make sure that it is less then 250
    if metadata['description'] is not None and (len(metadata['description']) > 250 or len(metadata['description']) == 0):
        print("Invalid size of description in the " + currentPath)
    else:
        project.description = metadata['description']

    # Set name and description
    project.name = metadata['name']

    # First we update the merge method setting
    project.merge_method = 'ff'
    # As well as the only allow merge if discussions are resolved setting
    project.only_allow_merge_if_all_discussions_are_resolved = True

    # Finally save the settings
    try:
        project.save()
    except:
        print ("Error saving project " + metadata['repopath'])

    # Start a list of known protection rules
    knownRules = []

    # Go over the protection rules we have and make sure they conform with the above
    for protected in project.protectedbranches.list():
        # Is it one of the ones we're allowed to have?
        if protected.name in branchPatternsToProtect:
            # Add it to the list so we can keep track
            knownRules.append( protected.name )
            # Then go on to the next one
            continue

        # Otherwise get rid of it
        protected.delete()

    # Now determine which ones we are missing
    missingRules = [ pattern for pattern in branchPatternsToProtect if pattern not in knownRules ]

    # Go over the ones we are missing
    for rule in missingRules:
        # And create them
        project.protectedbranches.create({
            'name': rule,
            'merge_access_level': gitlab.DEVELOPER_ACCESS,
            'push_access_level': gitlab.DEVELOPER_ACCESS
        })

# All done!
sys.exit(0)

