#!/usr/bin/python3
import os
import sys
import yaml
import argparse
import gitlab

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Updates the Gitlab group configuration based on repo-metadata')
parser.add_argument('--metadata-path', help='Path to the metadata to check', required=True)
parser.add_argument('--instance', help='Gitlab instance to work with', required=True)
parser.add_argument('--token', help='User token to use when authenticating with Gitlab', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.metadata_path ):
    print("Unable to locate specified metadata location: %s".format(args.metadata_path))
    sys.exit(1)

# Connect to Gitlab
gitlabServer = gitlab.Gitlab( args.instance, private_token=args.token )

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Do we have a group.yaml file?
    if 'group.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'group.yaml' )
    groupAvatar = os.path.join( currentPath, 'group.png' )
    metadataFile = open( metadataPath, 'r' )
    metadata = yaml.load( metadataFile )
    
    print("Processing " + metadata['name'])
    group = os.path.basename(os.path.normpath(currentPath))

    # Hack to set correct datatype for the group avatar
    gitlab.v4.objects.GroupManager._types = {'avatar': gitlab.types.ImageAttribute}

    # Get gitlabGroup
    gitlabGroup = gitlabServer.groups.get(group)

    # Set correct values and save
    gitlabGroup.name = metadata['name']
    gitlabGroup.description = metadata['description']
    print("Group avatar" + groupAvatar)
    gitlabGroup.avatar = open(groupAvatar, 'rb')
    gitlabGroup.save()

# All done!
sys.exit(0)

