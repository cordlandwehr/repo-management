#!/usr/bin/python3
import sys
import gitlab
import argparse

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Script to archive everything under unmaintained')
parser.add_argument('--gitlab', help='Gitlab instance to work with from config', required=True)
args = parser.parse_args()

# Connect to Gitlab
gitlabServer = gitlab.Gitlab.from_config( args.gitlab )

# Retrieve the group we are going to be working on
group = gitlabServer.groups.get( 'unmaintained' )
# Along with the projects in it
projects = group.projects.list( all = True )

# Start going over the various projects
for gProject in projects:
    # Retrieve an actual project instance we can work with
    project = gitlabServer.projects.get( gProject.id )

    print ("Archiving " + project.name )
    # Let's archive the repository
    project.archive()

# All done!
sys.exit(0)
