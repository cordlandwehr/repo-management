#!/usr/bin/python
import json
import os
import re

if 'GITALY_REPO' in os.environ:
    gitalyMetadata = json.loads( os.environ['GITALY_REPO'] )
    print( gitalyMetadata['glProjectPath'] )

else:
    path_match = re.match("^/srv/git/repositories/(.+).git$", os.getcwd())
    print( path_match.group(1) )

